#!/usr/bin/python

import requests
from http import HTTPStatus
import os
import datetime
import time

# Taken from https://meta.discourse.org/t/global-rate-limits-and-throttling-in-discourse/78612
MAX_ADMIN_API_REQS_PER_KEY_PER_MINUTE = 60
ONE_MINUTE_IN_SECONDS = 60
num_requests = 0

# Function to check the number of requests
# In case reached, sleep for 60 seconds
def check_max_requests():
  global num_requests
  num_requests+=1
  print("[num_req="+str(num_requests)+"]")
  # This will avoid to get 429 error code from requests, overcoming the max reqs per minute.
  if num_requests >= (MAX_ADMIN_API_REQS_PER_KEY_PER_MINUTE-1):
    print("--> Reached max requests. Sleeping "+str(ONE_MINUTE_IN_SECONDS)+" seconds...")
    time.sleep(ONE_MINUTE_IN_SECONDS)
    num_requests = 0

# Variable used for reporting
print(datetime.datetime.now())
print("[START]")

api_username = os.environ['API_USERNAME']
api_key = os.environ['API_KEY']
hostname = os.environ['HOSTNAME']

print("--> Using api_username: "+api_username)
print("--> Using hostname: "+hostname)
print("###############################################")

# DELETE ARCHIVED TOPICS
# This will modify the content of the archived topics/posts in order to avoid keep uploads.
archived_topics_url = 'https://'+hostname+'/latest.json?status=archived'
delete_topics_url = 'https://'+hostname+'/t/'

headers = {
  'Api-Key':api_key,
  'Api-Username':api_username
}

try:
  print("Fetching data from: "+archived_topics_url)
  response = requests.get(archived_topics_url,headers=headers)
  print("\t--> Status Code: "+str(response.status_code))
  check_max_requests()

  if response.status_code == HTTPStatus.OK:
    json_data = response.json()
    topics = json_data['topic_list']['topics']
    
    print("\t--> Number of fetched topics: "+str(len(topics)))

    for i in range(len(topics)):
      if(topics[i]['archived'] == True):
        # Get posts associated to this topic
        posts_url = 'https://'+hostname+'/t/'+str(topics[i]['slug'])+'/'+str(topics[i]['id'])+'.json'
        print('\tGetting posts from topic [id='+str(topics[i]['id'])+']\tURL: '+posts_url)
        posts_response = requests.get(posts_url,headers=headers)
        print("\t\t--> Status Code: "+str(posts_response.status_code))
        check_max_requests()

        if posts_response.status_code == HTTPStatus.OK:
          posts_json_data = posts_response.json()
          posts = posts_json_data['post_stream']['posts']

          print("\t--> Number of fetched posts: "+str(len(posts)))
          print(" ")

          # Modify the content of the posts in case they have any upload.
          # Unreferenced the upload, it will be automatically deleted by the system
          # Simple way it's to encapsulate content like in Markdown
          for j in range(len(posts)):
            post_to_modify_url = 'https://'+hostname+'/posts/'+str(posts[j]['id'])
            data = {
              'post[raw]':'```\n'+posts[j]['cooked']+'\n```'
            }

            print('\tUpdating post [id='+str(posts[j]['id'])+'] from topic [id='+str(topics[i]['id'])+'] \tURL: '+post_to_modify_url)
            update_posts_response = requests.put(post_to_modify_url,data=data,headers=headers)
            print("\t\t--> Status Code: "+str(update_posts_response.status_code))
            check_max_requests()

            # Detele posts associated to the topic
            post_to_delete_url = 'https://'+hostname+'/posts/'+str(posts[j]['id'])+'.json'
            print('\tDeleting post [id='+str(posts[j]['id'])+'] from topic [id='+str(topics[i]['id'])+'] \tURL: '+post_to_delete_url)
            delete_post_response = requests.delete(post_to_delete_url,headers=headers)
            # Can happen that the status code is 403, that means that is the first post = topic, so will be removed when the loop breaks.
            print("\t\t--> Status Code: "+str(delete_post_response.status_code))
            check_max_requests()
          # :for

        # Finally, deleting topics
        topic_to_delete_url = 'https://'+hostname+'/t/'+str(topics[i]['id'])+'.json'
        print('Deleting topic [id='+str(topics[i]['id'])+'] \tURL: '+topic_to_delete_url)
        delete_topic_response = requests.delete(topic_to_delete_url,headers=headers)
        print("\t--> Status Code: "+str(delete_topic_response.status_code))
        check_max_requests()
        print(" ")
  else:
    print("\t--> Status Code: "+str(response.status_code))
    print(response.json())

except Exception as e:
  print(e)

print("[END]")
print(datetime.datetime.now())
print(" ")
archived_url = 'https://'+hostname+'/latest?status=archived'
deleted_url = 'https://'+hostname+'/latest?status=deleted'
print("Check --> "+archived_url)
print("Check --> "+deleted_url)