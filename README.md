# Clean up archived posts
Cron job to be executed as part of the maintenance tools for Discourse CERN forums.
This script does:
- Traverse all archived posts under /latest?status=archived.
- Modify the content of every single post, in order to break up the link with the uploads.
- Move posts from archived to deleted, so no more visible for users (except admins), which means they will prevail on DB.

## How to deploy it
```oc process -f \template\discourse-tools-clean-up-archived-posts.yaml API_USERNAME='xxxx' API_KEY='xxxx' HOSTNAME='xxx.web.cern.ch' | oc create -f -```

## Misc
Get info about jobs: ```oc get cronjobs```

Edit specific job: ```oc edit cronjob clean-up-archived-posts```